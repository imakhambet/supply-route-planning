package enums;

/**
 * This enumeration represents task priorities
 */
public enum TaskPriority {
    HIGH, MEDIUM, LOW;

    public boolean isHigh() {
        return equals(HIGH);
    }

    public boolean isMedium() {
        return equals(MEDIUM);
    }

    public boolean isLow() {
        return equals(LOW);
    }

    public int getNumberValue() {
        switch (this) {
            case HIGH:
                return 3;
            case MEDIUM:
                return 2;
            case LOW:
                return 1;
        }
        return 0;
    }
}
