package enums;

/**
 * This enumeration represents vehicle types
 */
public enum VehicleType {
    CAR, SCOOTER, PUBLIC_TRANSPORT;

    public boolean isCar() {
        return equals(CAR);
    }

    public boolean isScooter() {
        return equals(SCOOTER);
    }

    public boolean isPublicTransport() {
        return equals(PUBLIC_TRANSPORT);
    }


}
