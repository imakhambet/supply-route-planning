package entities;

/**
 * This entity represents driver and his information
 */
public class Driver {

    private int id;
    private String name;
    private int hourlyRate;
    private int shiftStart;
    private int shiftEnd;

    public Driver() {
    }

    public Driver(int id, String name, int hourlyRate, int shiftStart, int shiftEnd) {
        this.id = id;
        this.name = name;
        this.hourlyRate = hourlyRate;
        this.shiftStart = shiftStart;
        this.shiftEnd = shiftEnd;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHourlyRate() {
        return hourlyRate;
    }

    public void setHourlyRate(int hourlyRate) {
        this.hourlyRate = hourlyRate;
    }

    public int getShiftStart() {
        return shiftStart;
    }

    public void setShiftStart(int shiftStart) {
        this.shiftStart = shiftStart;
    }

    public int getShiftEnd() {
        return shiftEnd;
    }

    public void setShiftEnd(int shiftEnd) {
        this.shiftEnd = shiftEnd;
    }

}
