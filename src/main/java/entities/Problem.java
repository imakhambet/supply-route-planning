package entities;

import enums.VehicleType;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Entity of problem with all needed information to solve
 */
public class Problem {
    private List<Vehicle> vehicles; //list of all vehicles
    private List<Driver> drivers; //list of all drivers
    private List<Location> locations; //list of all locations
    private List<DistanceMatrix> distanceMatrices; //all distance matrices
    private List<Task> tasks; //all tasks

    public Problem(List<Vehicle> vehicles, List<Driver> drivers, List<Location> locations,  List<DistanceMatrix> distanceMatrices, List<Task> tasks) {
        this.vehicles = vehicles;
        this.drivers = drivers;
        this.locations = locations;
        this.distanceMatrices = distanceMatrices;
        this.tasks = tasks;
    }

    public List<Vehicle> getVehicles() {
        return vehicles;
    }

    public void setVehicles(List<Vehicle> vehicles) {
        this.vehicles = vehicles;
    }

    public List<Driver> getDrivers() {
        return drivers;
    }

    public void setDrivers(List<Driver> drivers) {
        this.drivers = drivers;
    }

    public List<Location> getLocations() {
        return locations;
    }

    public void setLocations(List<Location> locations) {
        this.locations = locations;
    }

    private List<DistanceMatrix> getDistanceMatrices() {
        return distanceMatrices;
    }

    public void setDistanceMatrices(List<DistanceMatrix> distanceMatrices) {
        this.distanceMatrices = distanceMatrices;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    /**
     * Method for getting distances between all locations for given vehicle type
     *
     * @param vehicleType a type of vehicle
     * @return array of distances between office and all task locations
     */
    public DistanceMatrix getDistanceMatrixByVehicleType(VehicleType vehicleType) {
        return getDistanceMatrices().stream().filter(d -> d.getVehicleType().equals(vehicleType)).findFirst().orElse(null);
    }

    /**
     * Method for getting tasks that have given driver as required
     *
     * @param driver a driver to filter task locations
     * @return list of filtered task locations
     */
    public List<Task> getTasksByRequiredDriver(Driver driver) {
        return tasks.stream().filter(task -> task.getRequiredDrivers().contains(driver)).collect(Collectors.toList());
    }

    /**
     * Method for getting distance between locations of two given tasks for given vehicle type
     *
     * @param type   vehicle type
     * @param first  origin task
     * @param second destination task
     * @return distance
     */
    public int getDistance(VehicleType type, Task first, Task second) {
        DistanceMatrix matrix = getDistanceMatrixByVehicleType(type);
        if (first != null && second == null) {
            int locID = first.getLocation().getId();
            return matrix.getDistanceMatrixToOffice()[locID - 1];
        } else if (first == null && second != null) {
            int locID = second.getLocation().getId();
            return matrix.getDistanceMatrixToOffice()[locID - 1];
        }
        int locID1 = first.getLocation().getId();
        int locID2 = second.getLocation().getId();
        return matrix.getDistanceMatrixBetweenLocations()[locID1 - 1][locID2 - 1];
    }

    /**
     * Method for getting vehicle types of tasks that have given driver as required
     *
     * @param driver vehicle type
     * @return vehicle types
     */
    public List<VehicleType> getVehicleTypesByDriverTasks(Driver driver) {
        List<VehicleType> types = new ArrayList<>();
        List<Task> driverTasks = tasks.stream().filter(t -> t.getRequiredDrivers().contains(driver)).collect(Collectors.toList());
        for (Task task : driverTasks) {
            if (task.getRequiredVehicleTypes() != null) {
                types.addAll(task.getRequiredVehicleTypes());
            }
        }
        return types;
    }

}