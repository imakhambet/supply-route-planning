package entities;

import java.util.ArrayList;
import java.util.List;

/**
 * This entity represents route
 */
public class Route {

    private Long id; //id of route
    private Integer start; //time when driver leaves office
    private Integer end; //time when driver returns office
    private Driver driver; //assigned driver
    private Vehicle vehicle; //assigned vehicle
    private List<Task> tasks = new ArrayList<>(); //route's tasks
    private Problem problem;

    public Route() {
    }

    public Route(Problem problem, Driver driver, Vehicle vehicle) {
        this.problem = problem;
        this.driver = driver;
        this.vehicle = vehicle;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public Integer getEnd() {
        return end;
    }

    public void setEnd(Integer end) {
        this.end = end;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    public Problem getProblem() {
        return problem;
    }

    public void setProblem(Problem problem) {
        this.problem = problem;
    }

    /**
     * Method for getting last added task of route
     *
     * @return last task
     */
    public Task getLastTask() {
        if (tasks.size() < 1) {
            return null;
        }
        return getTasks().get(getTasks().size() - 1);
    }

    /**
     * Method for getting total time of route
     *
     * @return total time
     */
    public int getTotalRouteTime() {
        return this.getEnd() - this.getStart();
    }

    /**
     * Method for getting total travel time between all nodes of route
     *
     * @return total time
     */
    public int getTotalTravelTime() {
        int time = 0;
        time += problem.getDistanceMatrixByVehicleType(vehicle.getType()).getDistanceMatrixToOffice()[tasks.get(0).getLocation().getId() - 1];
        for (int i = 0; i < tasks.size() - 1; i++) {
            time += problem.getDistanceMatrixByVehicleType(vehicle.getType()).getDistanceMatrixBetweenLocations()[tasks.get(i).getLocation().getId() - 1][tasks.get(i + 1).getLocation().getId() - 1];
        }
        time += problem.getDistanceMatrixByVehicleType(vehicle.getType()).getDistanceMatrixToOffice()[tasks.get(tasks.size() - 1).getLocation().getId() - 1];
        return time;
    }

    /**
     * Method for getting total cost of route
     *
     * @return total cost of route
     */
    public double getTotalCost() {
        double vehiclePrice = getTotalTravelTime() * this.getVehicle().getHourlyRate();
        double driverPrice = getTotalRouteTime() / 60.0 * this.getDriver().getHourlyRate();

        return vehiclePrice + driverPrice;
    }

    /**
     * Method for getting average total cost
     *
     * @return average total cost
     */
    public double getAverageTotalCost() {
        double averageTotalCost = 0;
        if (tasks.size() > 0) {
                averageTotalCost = getTotalCost() / tasks.size();
        }
        return averageTotalCost;
    }

}
