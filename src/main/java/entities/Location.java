package entities;

/**
 * This entity represents location
 */
public class Location {

    private Integer id;
    private String name;
    private boolean office; //location is office or not

    public Location() {
    }

    public Location(Integer id, String name, boolean office) {
        this.id = id;
        this.name = name;
        this.office = office;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void printLocation() {
        System.out.println("Vertex " + id);
    }

    public boolean isOffice() {
        return office;
    }

    public void setOffice(boolean office) {
        this.office = office;
    }
}
