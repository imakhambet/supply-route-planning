package entities;

import java.util.List;

/**
 * Class represents solution that contains list of routes
 */
public class ProblemSolution {

    private List<Route> routes; //list of routes in which each task location of the problem is visited at most once
    private Problem problem; // solution's problem
    private List<Task> unroutedTasks; //all tasks without route

    public ProblemSolution(List<Route> routes, Problem problem, List<Task> unroutedTasks) {
        this.routes = routes;
        this.problem = problem;
        this.unroutedTasks = unroutedTasks;
    }

    public List<Route> getRoutes() {
        return routes;
    }

    public void setRoutes(List<Route> routes) {
        this.routes = routes;
    }

    public Problem getProblem() {
        return problem;
    }

    public void setProblem(Problem problem) {
        this.problem = problem;
    }

    public List<Task> getUnroutedTasks() {
        return unroutedTasks;
    }

    public void setUnroutedTasks(List<Task> unroutedTasks) {
        this.unroutedTasks = unroutedTasks;
    }


    /**
     * Method for getting solution cost
     *
     * @return solution cost
     */
    public double getSolutionCost() {
        int cost = 0;
        for (Route route: routes) {
            cost += route.getTotalCost();
        }
        return cost;
    }

    /**
     * Method for getting total time of all solution routes
     *
     * @return total time
     */
    public double getTotalTime() {
        int time = 0;
        for (Route route: routes) {
            time += route.getTotalRouteTime();
        }
        return time;
    }
}
