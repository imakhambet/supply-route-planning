package entities;

import enums.TaskPriority;
import enums.VehicleType;
import java.util.ArrayList;
import java.util.List;

/**
 * This entity represents task
 */
public class Task {
    private int id; //id of task
    private String name; //name of task
    private Location location; //location, where task is carried out
    private TaskPriority priority; //priority of task
    private int requiredTime; //required time to done this task
    private Integer timeWindowStart; //start time of time window
    private Integer timeWindowEnd; //end time of time window
    private List<Driver> requiredDrivers = new ArrayList<>(); //drivers that can perform this task
    private List<VehicleType> requiredVehicleTypes = new ArrayList<>(); //vehicle types that can service this task
    private Integer plannedStart; //service start time planned by the algorithm
    private Integer plannedEnd; //service end time planned by the algorithm


    public Task(int id, String name, Location location, TaskPriority priority, int requiredTime, Integer timeWindowStart, Integer timeWindowEnd) {
        this.id = id;
        this.location = location;
        this.name = name;
        this.priority = priority;
        this.requiredTime = requiredTime;
        this.timeWindowStart = timeWindowStart == null ? 0 : timeWindowStart;
        this.timeWindowEnd = timeWindowEnd == null ? 1440 : timeWindowEnd;
    }

    public Task(Task task) {
        this.id = task.getId();
        this.name = task.getName();
        this.location = task.getLocation();
        this.priority = task.getPriority();
        this.requiredTime = task.getRequiredTime();
        this.timeWindowStart = task.getTimeWindowStart();
        this.timeWindowEnd = task.getTimeWindowEnd();
        this.requiredDrivers = task.getRequiredDrivers();
        this.requiredVehicleTypes = task.getRequiredVehicleTypes();
        this.plannedStart = task.getPlannedStart();
        this.plannedEnd = task.getPlannedEnd();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public TaskPriority getPriority() {
        return priority;
    }

    public void setPriority(TaskPriority priority) {
        this.priority = priority;
    }

    public int getRequiredTime() {
        return requiredTime;
    }

    public void setRequiredTime(int requiredTime) {
        this.requiredTime = requiredTime;
    }

    public Integer getTimeWindowStart() {
        return timeWindowStart;
    }

    public void setTimeWindowStart(Integer timeWindowStart) {
        this.timeWindowStart = timeWindowStart;
    }

    public Integer getTimeWindowEnd() {
        return timeWindowEnd;
    }

    public void setTimeWindowEnd(Integer timeWindowEnd) {
        this.timeWindowEnd = timeWindowEnd;
    }

    public List<Driver> getRequiredDrivers() {
        return requiredDrivers;
    }

    public void setRequiredDrivers(List<Driver> requiredDrivers) {
        this.requiredDrivers = requiredDrivers;
    }

    public List<VehicleType> getRequiredVehicleTypes() {
        return requiredVehicleTypes;
    }

    public void setRequiredVehicleTypes(List<VehicleType> requiredVehicleTypes) {
        this.requiredVehicleTypes = requiredVehicleTypes;
    }

    public Integer getPlannedStart() {
        return plannedStart;
    }

    public void setPlannedStart(Integer plannedStart) {
        this.plannedStart = plannedStart;
    }

    public Integer getPlannedEnd() {
        return plannedEnd;
    }

    public void setPlannedEnd(Integer plannedEnd) {
        this.plannedEnd = plannedEnd;
    }

}
