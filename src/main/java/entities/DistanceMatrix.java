package entities;

import enums.VehicleType;

/**
 * This entity represents type of vehicle and distance matrix for this vehicle type
 */
public class DistanceMatrix {

    private VehicleType vehicleType;
    private int[][] distanceMatrix;

    public DistanceMatrix() {
    }

    public DistanceMatrix(VehicleType vehicleType, int[][] distationMatrix) {
        this.vehicleType = vehicleType;
        this.distanceMatrix = distationMatrix;
    }

    public VehicleType getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(VehicleType vehicleType) {
        this.vehicleType = vehicleType;
    }

    public int[][] getDistanceMatrix() {
        return distanceMatrix;
    }

    public void setDistanceMatrix(int[][] distanceMatrix) {
        this.distanceMatrix = distanceMatrix;
    }


    /**
     * Method for getting distances from/to office from/to all task locations
     *
     * @return array of distances between office and all task locations
     */
    public int[] getDistanceMatrixToOffice() {
        int[] distanceMatrixToOffice = new int[distanceMatrix.length - 1];
        if (distanceMatrix.length - 1 >= 0) {
            System.arraycopy(distanceMatrix[0], 1, distanceMatrixToOffice, 0, distanceMatrix.length - 1);
        }
        return distanceMatrixToOffice;
    }


    /**
     * Method for getting distances between all task locations
     *
     * @return double array of distances between all task locations
     */
    public int[][] getDistanceMatrixBetweenLocations() {
        int[][] distanceMatrixBetweenLocations = new int[distanceMatrix.length - 1][distanceMatrix.length - 1];
        for (int i = 1; i < distanceMatrix.length; i++) {
            System.arraycopy(distanceMatrix[i], 1, distanceMatrixBetweenLocations[i - 1], 0, distanceMatrix.length - 1);
        }
        return distanceMatrixBetweenLocations;
    }

}
