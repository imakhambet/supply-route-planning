package entities;

import enums.VehicleType;

/**
 * This entity represents vehicle
 */
public class Vehicle {
    private int id;
    private String name;
    private VehicleType type; //type of vehicle
    private double hourlyRate; //cost of vehicle use per hour

    public Vehicle() {
    }

    public Vehicle(int id, String name, String type, double hourlyRate) {
        this.id = id;
        this.name = name;
        this.type = VehicleType.valueOf(type);
        this.hourlyRate = hourlyRate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public VehicleType getType() {
        return type;
    }

    public void setType(VehicleType type) {
        this.type = type;
    }

    public double getHourlyRate() {
        return hourlyRate;
    }

    public void setHourlyRate(double hourlyRate) {
        this.hourlyRate = hourlyRate;
    }
}
