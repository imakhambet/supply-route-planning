import entities.*;
import entities.Problem;
import utils.ProblemReader;
import utils.ProblemSolver;

import java.io.File;
import java.io.IOException;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException {

        ClassLoader classLoader = Main.class.getClassLoader();

        //parse data from files
        List<Vehicle> vehicles = ProblemReader.vehicleReader(new File(Objects.requireNonNull(classLoader.getResource("vehicles.txt")).getFile()));
        List<Location> locations = ProblemReader.locationReader(new File(Objects.requireNonNull(classLoader.getResource("locations.txt")).getFile()));
        List<DistanceMatrix> distanceMatrices = ProblemReader.distanceMatricesReader(new File(Objects.requireNonNull(classLoader.getResource("distMatrices/")).getFile()));

        //select test mode
        Scanner scanner = new Scanner(System.in);  // Create a Scanner object
        System.out.println("Choose test mode:\nEnter 1 for 40 tasks and 2 drivers\nEnter 2 for 100 tasks and 5 drivers\nEnter 3 for 150 tasks and 7 drivers");
        int mode;
        String dir;
        try {
            mode = scanner.nextInt();  // Read user input
        } catch (Exception x) {
            throw new InputMismatchException();
        }
        switch (mode) {
            case 1:
                dir = "40";
                break;
            case 2:
                dir = "100";
                break;
            case 3:
                dir = "150";
                break;
            default:
                throw new UnsupportedOperationException();
        }

        //parse drivers and tasks for chosen test mode
        List<Driver> drivers = ProblemReader.driverReader(new File(Objects.requireNonNull(classLoader.getResource(dir + "/drivers.txt")).getFile()));
        List<Task> tasks = ProblemReader.taskReader(new File(Objects.requireNonNull(classLoader.getResource(dir + "/tasks.txt")).getFile()), drivers, locations);

        //create and solve problem
        Problem problem = new Problem(vehicles, drivers, locations, distanceMatrices, tasks);
        ProblemSolver solver = new ProblemSolver(problem); //create solver
        solver.routePlanningHeuristic(); //solve problem
    }
}
