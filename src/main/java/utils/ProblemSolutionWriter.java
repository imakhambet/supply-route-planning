package utils;

import entities.*;
import enums.VehicleType;

/**
 * Class for write solution
 */
public class ProblemSolutionWriter {

    private final static int MINUTES_IN_HOUR = 60;

    /**
     * Method for write given solution
     *
     * @param solution given solution
     */
    public static void solutionWriter(ProblemSolution solution) {
        String solutionInfo = "SOLUTION\n" +
                "Solution cost: " + solution.getSolutionCost() + "\n" +
                "Solution time: " + solution.getTotalTime() + "\n" +
                "Unrouted tasks: " + solution.getUnroutedTasks().size() + "\n";


        solutionInfo += getRoutesInfo(solution);
        solutionInfo += "\n" + getUnroutedTasks(solution);

        System.out.println(solutionInfo + "\n\n");
    }

    /**
     * Method for getting information about all routes of given solution
     *
     * @param solution given solution
     * @return string with information of all routes
     */
    private static String getRoutesInfo(ProblemSolution solution) {
        StringBuilder routesInfo = new StringBuilder();
        int i = 1;
        for (Route route : solution.getRoutes()) {
            routesInfo.append(getRouteInfo(route, i));
            routesInfo.append(getRoutePlan(route));
            i++;
        }
        return routesInfo.toString();
    }

    /**
     * Method for getting information about given route
     *
     * @param route given route
     * @return string with information about route
     */
    private static String getRouteInfo(Route route, int id) {
        return "\nRoute #" + id +
                "\nRoute time " + minsToTime(route.getStart()) + " - " + minsToTime(route.getEnd()) +
                "\nTasks number " + route.getTasks().size() +
                "\nDriver " + route.getDriver().getId() + " " + route.getDriver().getName() +
                "\nDriver shift " + minsToTime(route.getDriver().getShiftStart()) + "-" + minsToTime(route.getDriver().getShiftEnd()) +
                "\nVehicle " + route.getVehicle().getType() + " " + route.getVehicle().getId() + " " + route.getVehicle().getName() +
                "\nTotal cost " + route.getTotalCost() +
                "\nAverage total cost " + route.getAverageTotalCost() + "\n";
    }

    /**
     * Method for getting plan of given route
     *
     * @param route given route
     * @return string with route plan
     */
    private static String getRoutePlan(Route route) {
        VehicleType type = route.getVehicle().getType();
        Problem problem = route.getProblem();

        StringBuilder plan = new StringBuilder(("\nRoute plan: \n"));
        for (int i = 0; i < route.getTasks().size(); i++) {
            Task task = route.getTasks().get(i);
            int locID = task.getLocation().getId();
            if (i == 0) { //route from office
                int dist  = problem.getDistanceMatrixByVehicleType(type).getDistanceMatrixToOffice()[locID - 1];
                plan.append(dist).append(" min\n");
            }
            if (i != 0) { //route between tasks
                int dist = problem.getDistance(type, task, route.getTasks().get(i - 1));
                plan.append(dist).append(" min\n");
            }
            plan.append(getTaskInfo(task));
            if (i == route.getTasks().size() - 1) { //route to office
                int dist = problem.getDistanceMatrixByVehicleType(type).getDistanceMatrixToOffice()[locID - 1];
                plan.append(dist).append(" min\n");
            }
        }
        return plan.toString();
    }

    /**
     * Method for getting information of given task
     *
     * @param task given task
     * @return string of task information
     */
    private static String getTaskInfo(Task task) {
        return "Task ID " + task.getId() + "\t" +
                "Location ID " + task.getLocation().getId() + "\t" + task.getLocation().getName() + "\t" +
                "Priority " + task.getPriority() + "\t" +
                "Task time " + task.getRequiredTime() + "\t" +
                getTaskPlannedTime(task) +
                getTaskTimeWindow(task) +
                getRequiredDriversIds(task) +
                getRequiredVehiclesTypes(task)+"\n";
    }

    /**
     * Method for getting planned start and end time of task
     *
     * @param task given task
     * @return planned start and end time
     */
    private static String getTaskPlannedTime(Task task) {
        StringBuilder time = new StringBuilder();
        if (task.getPlannedStart() != null && task.getPlannedEnd() != null) {
            time.append("Planned ")
                    .append(minsToTime(task.getPlannedStart()))
                    .append("-")
                    .append(minsToTime(task.getPlannedStart() + task.getRequiredTime()))
                    .append("\t");
        }
        return time.toString();
    }

    /**
     * Method for getting time window of task
     *
     * @param task given task
     * @return time window start and end
     */
    private static String getTaskTimeWindow(Task task) {
        StringBuilder tw = new StringBuilder();
        if (task.getPlannedStart() != null && task.getPlannedEnd() != null) {
            tw.append("TW ")
                    .append(minsToTime(task.getTimeWindowStart()))
                    .append("-")
                    .append(minsToTime(task.getTimeWindowEnd()))
                    .append("\t");
        }
        return tw.toString();
    }

    /**
     * Method for getting IDs of task required drivers
     *
     * @param task given task
     * @return string of ids
     */
    private static String getRequiredDriversIds(Task task) {
        StringBuilder ids = new StringBuilder("Drivers ");
        for (Driver driver: task.getRequiredDrivers()) {
            ids.append(driver.getId()).append(" ");
        }
        return ids.append("\t").toString();
    }

    /**
     * Method for getting types of task required vehicles
     *
     * @param task given task
     * @return string of types
     */
    private static String getRequiredVehiclesTypes(Task task) {
        StringBuilder types = new StringBuilder("Vehicles ");
        for (VehicleType type: task.getRequiredVehicleTypes()) {
            types.append(type).append(" ");
        }
        return types.toString();
    }

    /**
     * Method for getting all tasks without route in current solution
     *
     * @param solution current solution
     * @return string with information of all unrouted tasks
     */
    private static String getUnroutedTasks(ProblemSolution solution) {
        StringBuilder unrouted = new StringBuilder();
        if (solution.getUnroutedTasks().size() > 0) {
            unrouted.append("Unrouted tasks:\n");
            for (Task task: solution.getUnroutedTasks()) {
                unrouted.append(getTaskInfo(task));
            }
        }
        return unrouted.toString();
    }

    /**
     * Method for convert time from minutes to HH:mm
     *
     * @param mins minutes from midnight
     * @return string of time
     */
    private static String minsToTime(int mins) {
        return String.format("%02d:%02d", mins / MINUTES_IN_HOUR, mins % 60);
    }
}
