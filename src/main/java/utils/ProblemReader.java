package utils;

import entities.*;
import enums.TaskPriority;
import enums.VehicleType;

import java.io.*;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Class for parse input information
 */
public class ProblemReader {

    private final static int MINUTES_IN_HOUR = 60;

    /**
     * Method for parse information about drivers from given file
     *
     * @param file a file with information about drivers
     * @return list of all parseable drivers
     */
    public static List<Driver> driverReader(File file) throws IOException {
        List<String> lines = getLines(file);
        List<Driver> drivers = new ArrayList<>();

        for (String line : lines) {
            String[] tokens = line.trim().split(" % ");
            Driver driver = new Driver(Integer.parseInt(tokens[0]), tokens[1], Integer.parseInt(tokens[2]), timeToMinutes(tokens[3]), timeToMinutes(tokens[4]));
            drivers.add(driver);
        }

        return drivers;
    }

    /**
     * Method for parse information about vehicles from given file
     *
     * @param file a file with information about vehicles
     * @return list of all parseable vehicle
     */
    public static List<Vehicle> vehicleReader(File file) throws IOException {
        List<Vehicle> vehicles = new ArrayList<>();
        List<String> lines = getLines(file);

        for (String line : lines) {
            String[] tokens = line.trim().split(" % ");
            Vehicle v = new Vehicle(Integer.parseInt(tokens[0]), tokens[1], tokens[2], Double.parseDouble(tokens[3]));
            vehicles.add(v);
        }

        return vehicles;
    }

    /**
     * Method for parse information about locations from given file
     *
     * @param file a file with information about locations
     * @return list of all parseable locations
     */
    public static List<Location> locationReader(File file) throws IOException {
        List<Location> locations = new ArrayList<>();
        List<String> lines = getLines(file);

        for (String line : lines) {
            String[] tokens = line.trim().split(" % ");
            boolean office = false;
            if (Integer.parseInt(tokens[0]) == 0) {
                office = true;
            }
            Location location = new Location(Integer.parseInt(tokens[0]), tokens[1], office);
            locations.add(location);
        }

        return locations;
    }

    /**
     * Method for parse distance matrices between locations for each vehicle type from given files
     *
     * @param folder a folder with files containing distance matrices
     * @return list of distance matrices for each vehicle type
     */
    public static List<DistanceMatrix> distanceMatricesReader(File folder) throws IOException {
        List<DistanceMatrix> distanceMatrices = new ArrayList<>();
        File[] listOfFiles = folder.listFiles();

        for (File file : Objects.requireNonNull(listOfFiles)) {
            if (file.isFile()) {
                DistanceMatrix distanceMatrix = new DistanceMatrix();
                if (file.getName().contains("car")) {
                    distanceMatrix.setVehicleType(VehicleType.CAR);
                } else if (file.getName().contains("scooter")) {
                    distanceMatrix.setVehicleType(VehicleType.SCOOTER);
                } else if (file.getName().contains("mhd")) {
                    distanceMatrix.setVehicleType(VehicleType.PUBLIC_TRANSPORT);
                }
                List<String> lines = getLines(file);
                int[][] distMatrix = new int[lines.size()][lines.size()];
                for (int i = 0; i < lines.size(); i++) {
                    String[] tokens = lines.get(i).trim().split(" ");
                    for (int j = 0; j < tokens.length; j++) {
                        distMatrix[i][j] = Integer.parseInt(tokens[j]);
                    }
                }
                distanceMatrix.setDistanceMatrix(distMatrix);
                distanceMatrices.add(distanceMatrix);
            }
        }

        return distanceMatrices;
    }

    /**
     * Method for parse lines from given file
     *
     * @param file a file with input information
     * @return list of file lines
     */
    private static List<String> getLines(File file) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(file));
        List<String> lines = new ArrayList<String>();
        String line;
        while ((line = br.readLine()) != null) {
            lines.add(line);
        }
        return lines;
    }

    /**
     * Method for parse information about tasks from given file
     *
     * @param file      a file with information about locations
     * @param drivers   all drivers
     * @param locations all locations
     * @return list of all parseable tasks
     */
    public static List<Task> taskReader(File file, List<Driver> drivers, List<Location> locations) throws IOException {
        List<Task> tasks = new ArrayList<>();
        List<String> lines = getLines(file);

        for (String line : lines) {
            String[] tokens = line.trim().split(" % ");

            Task task = new Task(Integer.parseInt(tokens[0]), tokens[1], locations.get(Integer.parseInt(tokens[2])),
                    TaskPriority.valueOf(tokens[3]), Integer.parseInt(tokens[4]), timeToMinutes(tokens[5]), timeToMinutes(tokens[6]));
            if (!tokens[7].equals("NULL")) {
                String[] driversID = tokens[7].split(",");
                for (String id : driversID) {
                    Driver driver = drivers.get(Integer.parseInt(id) - 1);
                    task.getRequiredDrivers().add(driver);
                }
            }
            if (!tokens[8].equals("NULL")) {
                String[] vehicleTypes = tokens[8].split(",");
                for (String typeStr : vehicleTypes) {
                    VehicleType type = VehicleType.valueOf(typeStr.trim());
                    task.getRequiredVehicleTypes().add(type);
                }
            }
            tasks.add(task);
        }

        return tasks;
    }

    /**
     * Method for convert time to minutes from midnight
     *
     * @param timeStr string with time
     * @return number of minutes from midnight
     */
    private static Integer timeToMinutes(String timeStr) {
        Integer minutes = null;
        if (!timeStr.equals("NULL")) {
            LocalTime time = LocalTime.parse(timeStr);
            minutes = time.getHour() * MINUTES_IN_HOUR + time.getMinute();
        }
        return minutes;
    }
}
