package utils;

import entities.*;
import enums.VehicleType;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Solver for route planning problem using Nearest neighbour heuristic
 */
public class ProblemSolver {

    private Problem problem; //route planning problem

    //coefficients for calculating the nearest
    private final double COEF_REQUIRED_DRIVER = 0.5;
    private final double COEF_REQUIRED_VEHICLE_TYPE = 0.5;
    private final double COEF_REQUIRED_BOTH = 0.4;
    private final double COEF_HIGH_PRIORITY = 0.5;
    private final double COEF_MEDIUM_PRIORITY = 0.7;
    private final double COEF_REMAINING_TIME_WINDOW = 0.05;

    public ProblemSolver(Problem problem) {
        this.problem = problem;
    }

    /**
     * Route planner using Nearest neighbour heuristic
     * This method creates solutions for given problem
     * Number of solutions equal number of sorted collections of drivers
     */
    public void routePlanningHeuristic() {
        List<Queue<Driver>> driverQueues = getDriverQueues(); //sorted collections of drivers

        //create and write solutions for each collection of drivers
        for (Queue<Driver> drivers : driverQueues) {
            List<Task> tasks = new ArrayList<>(problem.getTasks()); //remaining task locations for current solution
            List<Vehicle> vehicles = new ArrayList<>(problem.getVehicles()); //remaining vehicles for current solution

            ProblemSolution solution = solutionCreator(drivers, vehicles, tasks);
            ProblemSolutionWriter.solutionWriter(solution); //write current solution
        }
    }

    /**
     * Method to create solution for given drivers, vehicles and tasks
     *
     * @param tasks    all tasks of problem
     * @param drivers  all drivers of problem
     * @param vehicles all vehicles of problem
     * @return solution
     */
    private ProblemSolution solutionCreator(Queue<Driver> drivers, List<Vehicle> vehicles, List<Task> tasks) {
        List<Route> routes = new ArrayList<>(); //list of routes for current solution

        //create routes for current solution
        for (Driver driver : drivers) {
            if (tasks.size() == 0) {
                break; //stop build current solution, if number of remaining task locations is null
            }

            List<Route> allPossibleDriverRoutes = new ArrayList<>(); //all possible routes for current driver

            //get vehicle types and then vehicles of tasks where current driver is required
            List<VehicleType> driverVehTypes = problem.getVehicleTypesByDriverTasks(driver);
            List<Vehicle> driverVehicles = vehicles;
            if (driverVehTypes != null && driverVehTypes.size() > 0) {
                driverVehicles = vehicles.stream().filter(v -> driverVehTypes.contains(v.getType())).collect(Collectors.toList());
            }

            //create all possible routes for current driver with possible vehicles
            for (Vehicle vehicle : driverVehicles) {
                Route possibleRoute = routeCreator(tasks, driver, vehicle);

                //add route to possible routes, if route has tasks and vehicle is available
                if (possibleRoute.getTasks().size() > 0 && checkVehicleAvailability(possibleRoute, routes)) {
                    allPossibleDriverRoutes.add(possibleRoute);
                }
            }

            //find the best route from all possible routes of current driver
            if (allPossibleDriverRoutes.size() > 0) {
                //sort by routes by average total cost
                allPossibleDriverRoutes = allPossibleDriverRoutes.stream().sorted(Comparator.comparingDouble(Route::getAverageTotalCost)).collect(Collectors.toList());

                Route best = allPossibleDriverRoutes.get(0); //best route
                routes.add(best); //add best route to routes of current solution
                for (Task task : best.getTasks()) { //remove task locations of best route from remaining tasks
                    tasks.remove(tasks.stream().filter(t -> t.getId() == task.getId()).findFirst().orElse(null));
                }
                tasks.forEach(t -> {
                    t.setPlannedStart(null);
                    t.setPlannedEnd(null);
                });
            }
        }

        return new ProblemSolution(routes, problem, tasks); //write current solution
    }

    /**
     * Method to create route for given driver and vehicle from given tasks
     *
     * @param tasks   all available tasks for given driver and vehicle
     * @param driver  current driver
     * @param vehicle current vehicle
     * @return possible route
     */
    private Route routeCreator(List<Task> tasks, Driver driver, Vehicle vehicle) {
        //filter remaining locations for current driver and vehicle
        List<Task> availableTasks = tasks.stream().filter(t -> (
                t.getRequiredDrivers().contains(driver) || t.getRequiredDrivers().size() == 0)
                && (t.getRequiredVehicleTypes().contains(vehicle.getType()) || t.getRequiredVehicleTypes().size() == 0)
                && !(t.getTimeWindowStart() > driver.getShiftEnd() && driver.getShiftStart() > t.getTimeWindowEnd())
        ).collect(Collectors.toList());

        Route possibleRoute = new Route(problem, driver, vehicle); //route for current driver and vehicle
        //create route for current driver and current vehicle from remaining available locations
        while (availableTasks.size() > 0) {
            Task nearestNext = findNearestNeighbor(possibleRoute, availableTasks); //getting nearest neighbour for last location of current route
            if (nearestNext != null) {
                availableTasks.remove(nearestNext); //remove found location from
                //create new instance of task and add it to current route
                Task nearestNext2 = new Task(nearestNext);
                possibleRoute.getTasks().add(nearestNext2);
                //set start time to route
                if (possibleRoute.getStart() == null) {
                    int distFromOffice = problem.getDistance(vehicle.getType(), null, nearestNext);
                    int curRouteStart = nearestNext.getPlannedStart() - distFromOffice;
                    possibleRoute.setStart(curRouteStart);
                }
            } else {
                break; //break loop if route for current vehicle and driver is done
            }
        }
        //set end time for current route and add current route to driver's possible routes
        if (possibleRoute.getTasks().size() > 0) {
            Task lastTask = possibleRoute.getLastTask();
            int distToOffice = problem.getDistance(vehicle.getType(), lastTask, null);
            possibleRoute.setEnd(lastTask.getPlannedEnd() + distToOffice);
        }
        return possibleRoute;
    }

    /**
     * Method for find nearest task to office or last task of given route
     *
     * @param route current route for which the next location is searched
     * @param tasks all available tasks for current route
     * @return next task for current route
     */
    private Task findNearestNeighbor(Route route, List<Task> tasks) {
        Task lastTask = route.getLastTask();
        int lastTaskEndTime = (lastTask == null) ? route.getDriver().getShiftStart() : lastTask.getPlannedEnd(); //end time of last location or shift start time of current route driver

        double bestVal = Integer.MAX_VALUE;
        Task bestTask = null;
        int bestTaskVisitTime = -1;

        for (Task task : tasks) {
            int distance = (lastTask == null) ?
                    problem.getDistance(route.getVehicle().getType(), null, task) : problem.getDistance(route.getVehicle().getType(), lastTask, task); //get distance between last location of current route and current iterated location


            int minVisitTime = Math.max(task.getTimeWindowStart(), lastTaskEndTime + distance); //get visit time for current iterated location

            //break this iteration if service end time is later than time window end or after servicing current location, driver of current route doesn't have time to return to the office before the end of his shift
            if (minVisitTime + task.getRequiredTime() > task.getTimeWindowEnd() || minVisitTime + task.getRequiredTime() + problem.getDistance(route.getVehicle().getType(), task, null) > route.getDriver().getShiftEnd()) {
                continue;
            }

            //nearness of current task
            double nearness = getTaskNearness(route, task, minVisitTime, lastTaskEndTime, distance);

            //find minimal
            if (nearness < bestVal) {
                bestVal = nearness;
                bestTask = task;
                bestTaskVisitTime = minVisitTime;
            }
        }
        if (bestTask != null) { //set planned time of next selected location
            bestTask.setPlannedStart(bestTaskVisitTime);
            bestTask.setPlannedEnd(bestTaskVisitTime + bestTask.getRequiredTime());
        }
        return bestTask;
    }


    /**
     * Method to create list of sorted collections of drivers.
     * First collection of drivers is sorted by hourly rate
     * Second collection of drivers is sorted by number of task locations in which given driver is required
     *
     * @return list of sorted collections of drivers
     */
    private List<Queue<Driver>> getDriverQueues() {
        List<Queue<Driver>> driverQueues = new ArrayList<>();
        driverQueues.add(problem.getDrivers().stream().sorted(Comparator.comparingInt(Driver::getHourlyRate)).collect(Collectors.toCollection(LinkedList::new)));
        driverQueues.add(problem.getDrivers().stream().sorted(Comparator.comparingInt(d -> problem.getTasksByRequiredDriver(d).size())).collect(Collectors.toCollection(LinkedList::new)));
        return driverQueues;
    }


    /**
     * Method to calculate actual task's nearness from last task
     *
     * @param route           actual route
     * @param task            actual task
     * @param minVisitTime    minimal time when actual task can start
     * @param lastTaskEndTime end time of last task of actual route
     * @param distance        a distance from last task's location to actual task's location
     * @return nearness for actual task
     */
    private double getTaskNearness(Route route, Task task, int minVisitTime, int lastTaskEndTime, int distance) {
        double val = minVisitTime - lastTaskEndTime + distance;

        if (task.getPriority().isHigh()) {
            val *= COEF_HIGH_PRIORITY;
        } else if (task.getPriority().isMedium()) {
            val *= COEF_MEDIUM_PRIORITY;
        }

        val += (task.getTimeWindowEnd() - minVisitTime) * COEF_REMAINING_TIME_WINDOW; //recalculate with remaining time of time window's

        //recalculate, if task has required drivers
        if (task.getRequiredDrivers() != null && task.getRequiredVehicleTypes() == null) {
            if (task.getRequiredDrivers().contains(route.getDriver())) {
                val *= COEF_REQUIRED_DRIVER;
            }
        }

        //recalculate, if task has required vehicle types
        if (task.getRequiredVehicleTypes() != null && task.getRequiredDrivers() == null) {
            if (task.getRequiredVehicleTypes().contains(route.getVehicle().getType())) {
                val *= COEF_REQUIRED_VEHICLE_TYPE;
            }
        }

        //recalculate, if task has required drivers and required vehicle types
        if (task.getRequiredDrivers() != null && task.getRequiredVehicleTypes() != null) {
            if (task.getRequiredDrivers().contains(route.getDriver()) && task.getRequiredVehicleTypes().contains(route.getVehicle().getType())) {
                val *= COEF_REQUIRED_BOTH;
            }
        }
        return val;
    }


    /**
     * Method to check if actual possible route's vehicle is available for this route
     *
     * @param possibleRoute actual route
     * @param routes        actual task
     * @return true, if available
     */
    private boolean checkVehicleAvailability(Route possibleRoute, List<Route> routes) {
        if (possibleRoute.getVehicle().getType().isPublicTransport()) {
            return true;
        }
        for (Route r : routes) {
            if (r.getVehicle().equals(possibleRoute.getVehicle()) && r.getStart() < possibleRoute.getEnd() && possibleRoute.getStart() < r.getEnd()) {
                return false;
            }
        }
        return true;
    }
}